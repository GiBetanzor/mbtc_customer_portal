const axios = require('axios');


exports.homeRoutes = (req, res)=> {
    res.render('customer_login');
};

exports.new_sales_order = (req, res)=> {
    res.render('new_sales_order');
};

exports.sales_orders = (req, res)=> {
    // Make a get request to /api/sales-orders
    axios.get('http://localhost:8080/api/sales-orders')
        .then(function(response){
            res.render('sales_orders', {salesOrders: response.data});
        })
        .catch(err=>{
            res.send(err);
        });
};

exports.detailed_sales_order = (req, res)=> {
    // Make a get request to /api/sales-orders using sapSalesOrderNo
    axios.get('http://localhost:8080/api/sales-orders', {params: {id: req.query.sapSalesOrderNo}})
        .then(function(salesOrderData){
            res.render('detailed_sales_order', {salesOrders: salesOrderData.data});
        })
        .catch(err=>{
            res.send(err);
        });
};