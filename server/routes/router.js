const express = require('express');
const route = express.Router();

const services = require('../services/render');
const controller = require('../controller/controller');

/**
 * @description Root Route (Customer Login)
 * @method GET /
*/
route.get('/', services.homeRoutes);

/**
 * @description view all sales orders
 * @method GET /sales-orders
*/
route.get('/sales-orders', services.sales_orders);

/**
 * @description view detailed sales order using SAP Sales Order No.
 * @method GET /detailed-sales-order
*/
route.get('/detailed-sales-order', services.detailed_sales_order);

/**
 * @description make a new sales order
 * @method GET /new-sales-order
*/
route.get('/new-sales-order', services.new_sales_order);


// API
route.post('/api/sales-orders', controller.create);
route.get('/api/sales-orders', controller.find);

module.exports = route;