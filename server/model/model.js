const mongoose = require('mongoose');

var salesOrderSchema = new mongoose.Schema({
    
        accountName: {
            type: String,
            required: true
        },
        shipTo: {
            type: String,
            required: true
        },
        paymentTerms: {
            type: String,
            required: true
        },
        requestedDate: {
            type: Date, // date format 2021-10-27 23:30
            required: true
        },
        atp: {
            type: String,
            required: true
        },
        externalReference: String,
        comments: String,
        contactName: {
            type: String,
            required: true
        },
        phone: {
            type: String,
            required: true
        },
        currency: {
            type: String,
            required: true
        },
        postingDate: {
            type: String, // date format 2021-10-27 23:30
        },
        salesOrderId: {
            type: String,
        },
        sapSalesOrderNo: {
            type: String,
        },
        status: {
            type: String,
        },

        lineNo: {
            type: Number,
            required: true
        },
        productId:{
            type: String,
            required: true
        },
        productDescription: {
            type: String,
            required: true
        },
        quantity: {
            type: Number,
            required: true
        },
        unitMeasure: {
            type: String,
            required: true
        },
        personOrdered: {
            type: String,
        }
});

const SalesOrderDb = mongoose.model('sales_order', salesOrderSchema);

module.exports = SalesOrderDb;