var SalesOrderDb = require('../model/model');

// Create and save new sales order
exports.create = (req, res)=>{
    // validate request
    if(!req.body){
        res.status(400).send({message: "Field can not be empty!"});
        return;
    }

    // New sales order
    const salesOrder = new SalesOrderDb({
        accountName: req.body.accountName,
        shipTo: req.body.shipTo,
        paymentTerms: req.body.paymentTerms,
        requestedDate: req.body.requestedDate,
        atp: req.body.atp,
        externalReference: req.body.externalReference,
        comment: req.body.comment,
        contactName: req.body.contactName,
        phone: req.body.phone,
        currency: req.body.currency,
        postingDate: req.body.postingDate,
        sapSalesOrderNo: req.body.sapSalesOrderNo,
        status: req.body.status,
        lineNo: req.body.lineNo,
        productId: req.body.productId,
        productDescription: req.body.productDescription,
        quantity: req.body.quantity,
        unitMeasure: req.body.unitMeasure
    });

    // Save new sales order in the database
    salesOrder
    .save(salesOrder)
    .then(data=>{
        // res.send(data)
        res.redirect('/new-sales-order');
    })
    .catch(err=>{
        res.status(500).send({
            message: err.message || "Some error occured while creating a create operation"
        });
    });
};

// Retrieve and return all sales orders / Retrieve and return a single sales order
exports.find = (req, res)=>{

    // Retrieves a certain product in the database based on the product sapSalesOrderNo
    if(req.query.id){
        const id = req.query.id;

        SalesOrderDb.findById(id)
        .then(data=>{
            if(!data){
                res.status(404).send({
                    message: "Not found"});
            }else{
                res.send(data);
                }
        })
        .catch(err=>{
            res.status(500).send({
                message: `Error retrieving ${id}`
            });
        });

    // Retrieves all the products in the database
    }else{
        SalesOrderDb.find()
        .then(salesOrder=>{
            res.send(salesOrder);
        })
        .catch(err=>{
            res.status(500).send({
                message: err.message || "Error occured while retrieving sales order information"
            });
        });
    }
};